###################################################################################
# LAVA QA tool
# Copyright (C) 2015  Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import unittest
from lqa_tool.utils import merge_profiles

profile_1={
    'name': 'basic_profile',
    'path': '/some/path',
    'variables': {
        'a': 1,
        'b': 2
        }
    }

profile_2={
    'name': 'merged_profile',
    'variables': {
        'b': 9,
        'c': 3
        }
    }

main_profile={
    'name': 'main-profile',
    'template-dir': 'templates/',
    'templates': [ 'packages-tpl.json',
                   'validation-tpl.json',
                   'boot-tpl.json' ],
    'variables': {
        'imgpath': 'daily/1.0/',
        'release_version': '1.0',
        'stream': '/public/team/lqa/',
        'bootfstype': 'ext4'
        }
    }

sub_profile_1={
    'name': 'daily-i386-vm',
    'variables': {
        'pretty': "I386 VM image",
        'device_type': 'i386',
        'arch': 'i386',
        'board': 'i386',
        'fs': 'btrfs'
        }
    }

sub_profile_2={
    'name': 'daily-minnowboard-max',
    'variables': {
        'pretty': "Minnowboard max image",
        'device_type': 'minnowboard-max-E3825',
        'arch': 'amd64',
        'board': 'amd64-uefi',
        'fs': 'btrfs',
        'bootfstype': 'vfat'
        }
    }

nested_profile={
    'name': 'profile',
    'variables': {
        'templates': [ 'tpl_0', 'tpl_1', 'tpl_2' ],
        'sub_variables': {
            'sub_templates': [ 'tpl_0', 'tpl_1' ]
            },
        'streams': {
            'public': '/public/team/lqa/',
            'private': '/private/team/lqa/',
            'anonymous': '/anonymous/lqa/',
            'devel': {
                'anonymous': '/anonymous/devel/',
                'tests': '/public/tests/lqa/'
                }
            },
        'fs': 'ext2',
        'arch': 'i386'
        },
    'version': 1.0,
    'rev': 1
    }

nested_sub_profile={
    'name': 'devel_profile',
    'variables': {
        'sub_variables': {
            'sub_templates': [ 'tpl_3' ]
            },
        'streams': {
            'private': '/private/team/lqa/lqa-tests/',
            'devel': {
                'anonymous': '/anonymous/lqa-devel/',
                'private': '/private/team/lqa-devel/'
                }
            },
        'fs': 'ext4',
        },
    'version': 2.0
    }

class MergeProfiles(unittest.TestCase):

    def test_merge_profiles_basic(self):
        self.assertEqual(
            merge_profiles(profile_1, profile_2), 
            { 'name': 'merged_profile',
              'path': '/some/path',
              'variables': {
                    'a': 1,
                    'b': 9,
                    'c': 3
                    }
              }
            )

    def test_merge_profiles_main_with_sub_1(self):
        self.assertEqual(
            merge_profiles(main_profile, sub_profile_1), {
                'name': 'daily-i386-vm',
                'template-dir': 'templates/',
                'templates': [ 'packages-tpl.json',
                               'validation-tpl.json',
                               'boot-tpl.json' ],
                'variables': {
                    'pretty': "I386 VM image",
                    'device_type': 'i386',
                    'arch': 'i386',
                    'board': 'i386',
                    'fs': 'btrfs',
                    'imgpath': 'daily/1.0/',
                    'release_version': '1.0',
                    'stream': '/public/team/lqa/',
                    'bootfstype': 'ext4'
                    }
                })

    def test_merge_profiles_main_with_sub_2(self):
        self.assertEqual(
            merge_profiles(main_profile, sub_profile_2), {
                'name': 'daily-minnowboard-max',
                'template-dir': 'templates/',
                'templates': [ 'packages-tpl.json',
                               'validation-tpl.json',
                               'boot-tpl.json' ],
                'variables': {
                    'pretty': "Minnowboard max image",
                    'device_type': 'minnowboard-max-E3825',
                    'imgpath': 'daily/1.0/',
                    'release_version': '1.0',
                    'arch': 'amd64',
                    'board': 'amd64-uefi',
                    'fs': 'btrfs',
                    'stream': '/public/team/lqa/',
                    'bootfstype': 'vfat'
                    }
                })

    def test_merge_profiles_nested(self):
        self.assertEqual(
            merge_profiles(nested_profile, nested_sub_profile), {
                'name': 'devel_profile',
                'variables': {
                    'templates': [ 'tpl_0', 'tpl_1', 'tpl_2' ],
                    'sub_variables': {
                        'sub_templates': [ 'tpl_3' ]
                        },
                    'streams': {
                        'public': '/public/team/lqa/',
                        'private': '/private/team/lqa/lqa-tests/',
                        'anonymous': '/anonymous/lqa/',
                        'devel': {
                            'tests': '/public/tests/lqa/',
                            'anonymous': '/anonymous/lqa-devel/',
                            'private': '/private/team/lqa-devel/'
                            }
                        },
                    'fs': 'ext4',
                    'arch': 'i386'
                    },
                'version': 2.0,
                'rev': 1
                })
