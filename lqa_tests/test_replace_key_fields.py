###################################################################################
# LAVA QA tool
# Copyright (C) 2015  Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import json
import unittest

from lqa_tool.commands.submit import _replaceKeyFields

# Test a basic case of key replacement.
json_basic="""{
  "name": "basic",
  "params":
     {
       "sub_name": "sub_basic",
       "sub_option": "sub_foo"
     }
}
"""

json_basic_replaced="""{
  "name": "replaced",
  "params":
     {
       "sub_name": "sub_basic",
       "sub_option": "sub_bar"
     }
}
"""

# Test that numeric values are properly replaced.
json_num="""
{
  "image": "image.tar.gz",
  "version": 1.0,
  "partition": 2,
  "params": {
    "tests": {
      "test_0": [ "command_1", "command_2" ],
      "test_1": [ "command_3", "command_4" ]
    }
  }
}
"""

json_num_replaced="""
{
  "image": "image.tar.gz",
  "version": 2.6,
  "partition": 3,
  "params": {
    "tests": {
      "test_0": [ "command_5", "command_6" ],
      "test_1": [ "command_3", "command_4" ]
    }
  }
}
"""

# Test that replacing several values work well.
json_multiple="""
{
  "image": "image.tar.gz",
  "params": {
    "time": 1000,
    "time": 3000
  },
  "params": {
    "time": 200
  }
}
"""

json_multiple_replaced="""
{
  "image": "image.tar.gz",
  "params": {
    "time": 4000,
    "time": 4000
  },
  "params": {
    "time": 4000
  }
}
"""

# Test a complete example for a job template.
json_job="""
{
  "timeout": 3600, 
  "actions": [
    {
      "command": "deploy_image", 
      "parameters": {
        "username": "{{username}}", 
        "image": "https://images.collabora.co.uk/singularity/daily/ceti/150625-004801/singularity-ceti-i386-core-i386_150625-004801-collabora-3.0.ext4.img.gz", 
        "login_prompt": "login:", 
        "password": "{{password}}", 
        "login_commands": [
          "{{command}}"
        ], 
        "password_prompt": "Password:"
      }
    },
    {
      "command": "lava_test_shell", 
      "parameters": {
        "timeout": 1800,
        "testdef_repos": [
          {
            "git-repo": "git://git.collabora.co.uk/git/singularity/qa/test-definitions.git", 
            "testdef": "singularity-validation/basic-commands.yaml"
          }, 
          {
            "git-repo": "git://git.collabora.co.uk/git/singularity/qa/test-definitions.git", 
            "testdef": "singularity-validation/package-installation.yaml"
          }, 
          {
            "git-repo": "git://git.collabora.co.uk/git/singularity/qa/test-definitions.git", 
            "testdef": "singularity-validation/systemd-analyze.yaml"
          }
        ]
      }
    }, 
    {
      "command": "submit_results", 
      "parameters": {
        "stream": "/this/stream/will/be/replaced/", 
        "server": "https://lava.collabora.co.uk/RPC2/"
      }
    }
  ], 
  "job_name": "singularity-validation", 
  "device_type": "i386"
}
"""

json_job_replaced="""
{
  "timeout": 3600, 
  "actions": [
    {
      "command": "deploy_image", 
      "parameters": {
        "username": "collabora", 
        "image": "https://images.collabora.co.uk/singularity/daily/ceti/150625-004801/singularity-ceti-i386-core-i386_150625-004801-collabora-3.0.ext4.img.gz", 
        "login_prompt": "login:", 
        "password": "collabora", 
        "login_commands": [
          "sudo su"
        ], 
        "password_prompt": "Password:"
      }
    }, 
    {
      "command": "lava_test_shell", 
      "parameters": {
        "timeout": 1800,
        "testdef_repos": [
          {
            "git-repo": "git://git.collabora.co.uk/git/singularity/qa/test-definitions.git", 
            "testdef": "singularity-validation/basic-commands.yaml"
          }, 
          {
            "git-repo": "git://git.collabora.co.uk/git/singularity/qa/test-definitions.git", 
            "testdef": "singularity-validation/package-installation.yaml"
          }, 
          {
            "git-repo": "git://git.collabora.co.uk/git/singularity/qa/test-definitions.git", 
            "testdef": "singularity-validation/systemd-analyze.yaml"
          }
        ]
      }
    },
    {
      "command": "submit_results", 
      "parameters": {
        "stream": "/public/personal/user/", 
        "server": "https://lava.collabora.co.uk/RPC2/"
      }
    }
  ], 
  "job_name": "singularity-validation", 
  "device_type": "i386"
}
"""

class ReplaceKeyFields(unittest.TestCase):

    def test_replace_key_fields_basic(self):
        self.assertEqual(
            _replaceKeyFields(json.loads(json_basic),
                              { 'name': 'replaced',
                                'sub_option': 'sub_bar' }),
            json.loads(json_basic_replaced))

    def test_replace_key_fields_version(self):
        self.assertEqual(
            _replaceKeyFields(json.loads(json_num),
                              { 'version': 2.6,
                                'partition': 3,
                                'test_0': [ "command_5", "command_6" ] }),
            json.loads(json_num_replaced))

    def test_replace_key_fields_multiple(self):
        self.assertEqual(
            _replaceKeyFields(json.loads(json_multiple), { 'time': 4000 }),
            json.loads(json_multiple_replaced))

    def test_replace_key_fields_job(self):
        self.assertEqual(
            _replaceKeyFields(json.loads(json_job),
                              { 'username': 'collabora',
                                'password': 'collabora',
                                'login_commands': [ "sudo su" ],
                                'stream': "/public/personal/user/"}),
            json.loads(json_job_replaced))
