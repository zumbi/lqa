###################################################################################
# LAVA QA tool
# Copyright (C) 2015, 2016 Collabora Ltd.

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

from lqa_api.job import Job, JobError
from lqa_api.exit_codes import APPLICATION_ERROR
from lqa_tool.settings import lqa_logger
from lqa_tool.commands import Command

class ResultsCmd(Command):

    def __init__(self, args):
        Command.__init__(self, args)

    def run(self):
        """Fetch the result for job id list"""

        for job_id in self.job_ids:
            try:
                job = Job(job_id, self.server)
                for result_line in job.results:
                    print(result_line)
            except JobError as err:
                lqa_logger.error("results command: {}".format(err))
                exit(APPLICATION_ERROR)
